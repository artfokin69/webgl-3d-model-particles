#pragma glslify: perlin2d = require(glsl-noise/simplex/2d)
uniform float time;
uniform float progress;
uniform vec4 resolution;
uniform float fqAvg;
uniform sampler2D tAudioData;
uniform bool animateColor;
uniform vec3 transfusionColor0;
uniform vec3 transfusionColor1;
uniform vec3 transfusionColor2;
uniform float transfusionColorIntensity0;
uniform float transfusionColorIntensity1;
uniform float transfusionColorIntensity2;
uniform vec3 activeColorFrom;
uniform vec3 activeColorTo;

varying vec2 vUv;
varying vec3 vPosition;
varying vec3 vNormal;

void main()	{
	float noise = perlin2d(vNormal.xy + vNormal.xy * fqAvg / 90. + vec2(time / 10.));
	float dist = length(gl_PointCoord - vec2(0.5));
	float disc = smoothstep(0.5, 0.45, dist);
	float colorMixer = perlin2d(vPosition.xy + vec2(time));
	vec3 color;
	if(fqAvg > 40. && colorMixer < 0.5 && animateColor){
		color = mix(activeColorFrom, activeColorTo, (fqAvg / 90.));
	}
	else if(colorMixer < transfusionColorIntensity0){
		color = transfusionColor0;
	} else if(colorMixer < transfusionColorIntensity1) {
		color = transfusionColor1;
	} else {
		color = transfusionColor2;
	}
	color = mix(activeColorFrom, activeColorTo, (fqAvg / 90.));
	gl_FragColor = vec4(color, disc * noise);
	if(disc < 0.001) discard;
}