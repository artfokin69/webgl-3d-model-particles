import autoBind from 'auto-bind';
import {GUI} from 'dat.gui'
import { PerspectiveCamera, Scene, Vector2, WebGLRenderer, Clock } from "three";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';
import nouseVertShader from '../assets/shaders/noise.vert';
import nouseFragShader from '../assets/shaders/noise.frag';


/* 
  setup: 
  import and init class instance
  add onResize handler
  add onRender handler
  add setupGui handler
  remove 'this.renderer.render()' method
*/

export default class PostprocessingGroup{
  composer: EffectComposer;
  renderScene: RenderPass;
  bloomPass: UnrealBloomPass;
  noisePass: ShaderPass;
  constructor(scene:Scene, camera:PerspectiveCamera ,renderer: WebGLRenderer){
    autoBind(this)
    this.renderScene = new RenderPass(scene, camera);
    this.bloomPass = new UnrealBloomPass(new Vector2( window.innerWidth, window.innerHeight ), 0.3, 0.3, 0.4 );
    this.noisePass = new ShaderPass({
      uniforms: {
        "tDiffuse": { value: null },
        "amount": { value: 0.0 }
      },
      vertexShader: nouseVertShader,
      fragmentShader: nouseFragShader
    })
    this.composer = new EffectComposer(renderer);
    this.composer.addPass(this.renderScene);
    // this.composer.addPass(this.bloomPass);
    this.composer.addPass(this.noisePass);
  }

  onResize(){
    this.composer.setSize(window.innerWidth, window.innerHeight);
  }
  onRender(clock: Clock){
    this.noisePass.uniforms["amount"].value += 0.1;
    this.composer.render();
  }
  setSize(w:number,h:number){

  }
  setupGui(gui: GUI){
    const rootFolder = gui.addFolder('Postprocessing')
    rootFolder.add({'enabled': true}, 'enabled').onChange(enabled => this.composer.passes.forEach(pass=>{
      if(!(pass instanceof RenderPass)) {
        pass.enabled = enabled;
      }
    }))
    const bloomPassFolder = rootFolder.addFolder('Bloom');
    bloomPassFolder.add(this.bloomPass, 'enabled').listen();
    bloomPassFolder.add(this.bloomPass, 'threshold', 0, 1, 0.01);
    bloomPassFolder.add(this.bloomPass, 'strength', 0, 3, 0.01);
    bloomPassFolder.add(this.bloomPass, 'radius', 0, 1, 0.01);

    const nousePassFolder = rootFolder.addFolder('Noise');
    nousePassFolder.add(this.noisePass, 'enabled').listen();

    const greyscaleFolder = rootFolder.addFolder('Greyscale');
  }
}