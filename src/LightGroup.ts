import { GUI } from "dat.gui";
import { DirectionalLight, MathUtils, Mesh, MeshBasicMaterial, PointLight, Scene, SphereGeometry, SpotLight, SpotLightHelper, Clock, AmbientLight } from "three";
import { DegRadHelper } from "./guiHelpers";
import autoBind from 'auto-bind';


export default class LightGroup{
  light: SpotLight;
  lightHelper: SpotLightHelper;
  pointLight1: PointLight;
  ambientLight: AmbientLight;
  constructor(scene: Scene){
    autoBind(this);
    this.light = new SpotLight( 0xffffff, 1);
    this.light.target.position.set(0,0,0);
    this.light.position.set(0, 30, 30)
    this.light.angle = MathUtils.degToRad(38)
    this.light.penumbra = 1;
    // this.light.angle = MathUtils.degToRad(26);
    this.light.intensity = 2;
    this.light.angle = 34;
    this.light.castShadow = true
    scene.add(this.light)

    this.lightHelper = new SpotLightHelper(this.light);
    this.light.add( new Mesh( new SphereGeometry( 1, 32, 32 ), new MeshBasicMaterial( { color: 0xcddc39 } ) ) );
    // scene.add(this.lightHelper)
    this.lightHelper.update();
    
    
    this.pointLight1 = new PointLight(0xffffff, 0.8, 400)
    this.pointLight1.position.set(0, 28, 0)
    // this.pointLight1.add( new Mesh( new SphereGeometry( 1, 32, 32 ), new MeshBasicMaterial( { color: 0xff0000 } ) ) );
    scene.add(this.pointLight1);

    this.ambientLight = new AmbientLight( 0x404040 );
    scene.add( this.ambientLight );
  }
  onRender(clock: Clock){
    const t = clock.getElapsedTime();
    this.light.position.x = 30 * Math.sin(t*0.4);
    this.light.position.z = 30 * Math.cos(t*0.4);
    this.light.position.y = 10 + 20 * ((1 + Math.cos(t*0.6)) * 0.5)
    this.light.lookAt(0,5,0);
    this.lightHelper.update()
  }
  setupGui(gui: GUI){
    const folder = gui.addFolder("Light");
    const visibleControler = folder.add({'Visible': true}, 'Visible');
    visibleControler.onChange(visible=>{
      this.light.visible = visible;
      this.pointLight1.visible = visible;
      this.lightHelper.visible = visible;
      this.ambientLight.visible = visible;
    })
    visibleControler.setValue(false);
    
    this.makeLightGUI(folder, this.light, 'Spot Light with shadows', undefined, ()=>this.lightHelper.update());
    this.makeLightGUI(folder, this.pointLight1, 'Point Light 1');
    this.makeLightGUI(folder, this.ambientLight, 'Ambient Light');
    
  }
  makeLightGUI(gui: GUI, light: THREE.Light, name:string, radius=100, onChange: (p:number)=>void = (p)=>{}) {
    const folder = gui.addFolder(name);
    folder.add(light, 'visible').listen();
    folder.add(light.position, 'x', -radius, radius).onChange(onChange);
    folder.add(light.position, 'y', 0, radius).onChange(onChange);
    folder.add(light.position, 'z', -radius, radius).onChange(onChange);
    folder.add(light, 'intensity', 0, 10, 0.1).onChange(onChange);
    if(light instanceof PointLight){
      folder.add(light, 'distance', 0, 1000, 1).onChange(onChange);
    }
    if(light instanceof DirectionalLight){
      const onTargeChange = (p: number)=>{
        light.target.updateMatrixWorld();
        onChange(p)
      }
      folder.add(light.target.position, 'x', -10, 10, 0.1).onChange(onTargeChange);
      folder.add(light.target.position, 'y', 0, 10, 0.1).onChange(onTargeChange);
      folder.add(light.target.position, 'z', -10, 10, 0.1).onChange(onTargeChange);
    }
    if(light instanceof SpotLight){
      folder.add(new DegRadHelper(light, 'angle'), 'value', 0, 90).name('angle').onChange(onChange);
      folder.add(light, 'penumbra', 0, 1, 0.01).onChange(onChange);
    }
  
    return folder;
  }
}