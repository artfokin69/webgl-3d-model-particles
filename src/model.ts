import { Group, Scene, Clock, ShaderMaterial, DoubleSide, Vector4, Vector2, Mesh, Material, Object3D, MeshMatcapMaterial, Texture, Points, Vector3, BufferGeometry, Float32BufferAttribute, BufferAttribute, AdditiveBlending, Color, MathUtils, MeshBasicMaterial, MeshLambertMaterial, MeshPhongMaterial, Raycaster, MeshStandardMaterial, WebGLRenderer, DataTexture, Audio, AudioAnalyser, RedFormat, LuminanceFormat, AudioListener, Shader} from "three";
import {SceneUtils} from 'three/examples/jsm/utils/SceneUtils.js';
import { MeshSurfaceSampler } from 'three/examples/jsm/math/MeshSurfaceSampler.js';
import extendMaterial from './plugins/extendMaterial';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import autoBind from 'auto-bind';
import {loadScene, loadTexture, loadAudio} from './helpers';
import wavesPollenFragmentShader from '../assets/shaders/wavesPollen.frag';
import solidPollenFragmentShader from '../assets/shaders/solidPollen.frag';
import pollenVertexShader from '../assets/shaders/pollen.vert';
import {GUI} from 'dat.gui';
import audioPath from '../assets/music/sound4.mp3';
import modelPath from '../assets/models/sphere.glb';

import { Inertia } from "./helpers/inertia";

type vertexShaderType = 'waves' | 'solid';

export default class Model{
  gltf: GLTF;
  figureMesh: Object3D;
  surfaces: Points<BufferGeometry, ShaderMaterial>[] = [];
  loaded = false;
  isInit = false;
  matcap1: Texture;
  matcap2: Texture;
  shaderMaterial: ShaderMaterial;
  hoverProgress = 0;
  audioBuffer: AudioBuffer;
  audio: Audio<GainNode>;
  audioListener: AudioListener;
  audioAnalyser: AudioAnalyser;
  fftSize: 64;
  soundUniforms = {tAudioData: {value: DataTexture}, fqAvg: {value:0}};
  audioFqAvg = 0;
  audioFqAvgInertia = new Inertia(0, 100, 0.4, 0.4, 0);
  maxAudioAvg = 0;
  settings = {
    animatePosition: true,
    animateColor: true,
    colors:{
      transfusion:[
        {
          color: 'rgb(255,215,0)',
          intensity: 0.5,
        },
        {
          color: 'rgb(238,232,170)',
          intensity: 0.8,
        },
        {
          color: 'rgb(255, 255, 255)',
          intensity: 1
        }
      ],
      active:{
        from: {
          color: 'rgb(50, 37, 222)',
          intensity: 0.2
        },
        to: {
          color: 'rgb(108,99,223))',
          intensity: 0.2
        } 
      }
    }
  }
  constructor(){
    autoBind(this);
  }
  async load(){
    const [gltf,  music] = await Promise.all([
      loadScene(modelPath), 
      loadAudio(audioPath),
    ])
    this.gltf = gltf;
    this.audioBuffer = music;
    this.loaded = true;
  }

  init(scene: Scene, renderer: WebGLRenderer){
    this.audioUnfiroms = this.addAudio(renderer);
    this.figureMesh = this.gltf.scene.children[0];
    this.makeSurfaces(this.figureMesh, scene);
    this.isInit = true;
  }

  makeSurfaces(parent: Object3D, scene: Scene){
    const surfaces: Points<BufferGeometry, ShaderMaterial>[] = [];
    const meshes: Mesh[] = [];
    parent.traverse((node:Mesh)=>{
      if(node.isMesh){
        meshes.push(node);
        node.rotation.copy(parent.rotation);
        node.position.copy(parent.position);
        node.matrixWorldNeedsUpdate = true;
        node.material = new MeshStandardMaterial({color: 0x000000});
        const surface = this.makeSurfaceMesh(node);
        surface.rotation.copy(node.rotation);
        surface.position.copy(node.position);
        surface.matrixWorldNeedsUpdate = true;
        surfaces.push(surface);
      }
    })
    this.surfaces = surfaces;
    [...meshes,...this.surfaces].forEach(m=>{scene.add(m)});
  }

  makeSurfaceMesh(mesh: Mesh){
    const type = mesh.name as vertexShaderType;
    const material = this.makeMaterial(type);
    const geometry = new BufferGeometry()
    const sampler = new MeshSurfaceSampler(mesh)
	    .setWeightAttribute( 'color' )
      .build();
    const number = type === 'solid' ? 30000 : 50000;
    const pointPos = new Float32Array(number * 3);
    const normals = new Float32Array(number * 3);
    const sizes = new Float32Array(number);

    for (let index = 0; index < number; index++) {
      const _position = new Vector3();
      const _normal = new Vector3();
      sampler.sample(_position, _normal);
      pointPos.set([_position.x,_position.y, _position.z], index * 3);
      
      normals.set([_normal.x, _normal.y, _normal.z], index * 3);
      sizes.set([Math.random()], index);
    }
    geometry.setAttribute('position', new  BufferAttribute(pointPos, 3)); 
    geometry.setAttribute('normal', new  BufferAttribute(normals, 3)); 
    geometry.setAttribute('size', new  BufferAttribute(sizes, 1)); 
    const surface = new Points(geometry, material);
    surface.rotation.copy(mesh.rotation)
    surface.matrixWorldNeedsUpdate = true;
    return surface;
  }

  makeMaterial(figureType:vertexShaderType){
    const shaderByType: Record<vertexShaderType, string> = {
      waves: wavesPollenFragmentShader,
      solid: solidPollenFragmentShader,
    }
    const colorUniforms = {
      activeColorFrom: {value: new Color(this.settings.colors.active.from.color)},
      activeColorTo: {value: new Color(this.settings.colors.active.to.color)},
    };
    this.settings.colors.transfusion.forEach((color,index)=>{
      colorUniforms[`transfusionColor${index}`] = {value: new Color(color.color)};
      colorUniforms[`transfusionColorIntensity${index}`] = {value: color.intensity};
    });
    return new ShaderMaterial({
      extensions: {
        derivatives: true,
      },
      side: DoubleSide,
      uniforms: {
        time: { value: 0 },
        resolution: { value: new Vector4() },
        uvRate1: {
          value: new Vector2(1, 1)
        },
        hoverProgress: {
          value: 0,
        },
        animatePosition: {
          value: true
        },
        animateColor: {
          value: true,
        },
        ...colorUniforms,
        ...this.soundUniforms,
      },
      // wireframe: true,
      transparent: true,
      // blending: AdditiveBlending,
      // depthTest: false,
      depthWrite: false,
      vertexShader: pollenVertexShader,
      fragmentShader: shaderByType[figureType],
    });
  }

  addAudio(renderer: WebGLRenderer){
    this.audioListener = new AudioListener();
    this.audio = new Audio(this.audioListener);
    this.audio.setBuffer( this.audioBuffer );
    this.audio.setLoop( true );  
    this.audioAnalyser = new AudioAnalyser(this.audio, this.fftSize);
    const format = ( renderer.capabilities.isWebGL2 ) ? RedFormat : LuminanceFormat;
    return {
      tAudioData: { 
        value: new DataTexture( this.audioAnalyser.data, this.fftSize / 2, 1, format ) 
      },
      fqAvg: {
        value: 0,
      }
    };
  }
    
  onResize(w:number,h:number){
    
  }

  onRender(clock: Clock, raycaster: Raycaster){
    const intersects = raycaster.intersectObject(this.figureMesh, true);
    const time = clock.getElapsedTime();
    const delta = clock.getDelta();
    this.surfaces.forEach(surface=>{
      surface.material.uniforms.time.value = time;
    })
    
    this.audioAnalyser.getFrequencyData())
    this.audioFqAvg = this.audioAnalyser.getAverageFrequency();
    this.audioFqAvgInertia.update(this.audioFqAvg)
    this.soundUniforms.fqAvg.value = this.audioFqAvgInertia.value;
    this.soundUniforms.tAudioData.value.needsUpdate = true;
  }

  setupGui(gui: GUI){
    gui.add({play: false}, 'play').onChange(play=>play ? this.audio.play() : this.audio.pause());
    const inertiaFolder = gui.addFolder('Inertia');
    inertiaFolder.open();
    inertiaFolder.add(this.audioFqAvgInertia, 'acc', 0, 1, 0.01 );
    inertiaFolder.add(this.audioFqAvgInertia, 'friction', 0, 1, 0.01 );

    gui.add(this.settings, 'animatePosition').onChange(animate=>{
      this.surfaces.forEach(surface=>{
        surface.material.uniforms.animatePosition.value = animate;
      })
    })
    gui.add(this.settings, 'animateColor').onChange(animate=>{
      this.surfaces.forEach(surface=>{
        surface.material.uniforms.animateColor.value = animate;
      })
    })

    const colorFolder = gui.addFolder('Colors');
    colorFolder.open();
    this.settings.colors.transfusion.forEach((color,index)=>{
      this.addGuiColor(colorFolder, color, 'Color', index);
    })
    colorFolder.addColor(this.settings.colors.active.from, 'color').name('Active from').onChange(color=>{
      this.surfaces.forEach(surface=>{
        surface.material.uniforms.activeColorFrom.value = new Color(color);
      })
    });
    colorFolder.addColor(this.settings.colors.active.to, 'color').name('Active to').onChange(color=>{
      this.surfaces.forEach(surface=>{
        surface.material.uniforms.activeColorTo.value = new Color(color);
      })
    });
  }
  addGuiColor(gui: GUI, color: {color: string, intensity: number}, name?: string, index: number){
    gui.addColor(color, 'color').name(`${name} ${index}`).onChange((color)=>{
      this.surfaces.forEach(surface=>{
        surface.material.uniforms[`transfusionColor${index}`].value = new Color(color);
      })
    });
    gui.add(color, 'intensity',  0, 1, 0.1).name(`Intensity ${index}`).onChange(intensity=>{
      console.log(`transfusionColorIntensity${index} = ${intensity}`);
      this.surfaces.forEach(surface=>{
        surface.material.uniforms[`transfusionColorIntensity${index}`].value = intensity;
      })
    });
  }
}